import * as _ from 'lodash';
export default function parseQueryClone(query) {
    return _.cloneDeep(query);
}
