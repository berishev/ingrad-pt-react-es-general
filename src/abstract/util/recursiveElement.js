export function getRecursiveEl(element, className) {
    function load(element) {
        if (element == null)
            return null;
        let parent = element.parentNode;
        if (parent == null)
            return null;
        if (parent.isEqualNode(document.body))
            return null;
        if (parent['className'] == className)
            return parent;
        return load(parent);
    }
    return load(element);
}
