import { LINQ } from 'berish-linq/dist';
export const ValidationModelSymbol = Symbol('[ValidationModelSymbol]');
class ValidationInfoModel {
    constructor() {
        this.filters = [];
        this.isRequired = false;
    }
    static createInstance(info) {
        if (info) {
            if (info instanceof ValidationInfoModel)
                return info;
            return Object.assign(new ValidationInfoModel(), info);
        }
        return new ValidationInfoModel();
    }
    getOrAddFilter(filter) {
        let index = this.filters.indexOf(filter);
        if (index === -1)
            index = this.filters.push(filter) - 1;
        return this.filters[index];
    }
}
class FilterModel {
    constructor(condition) {
        this.errorText = 'Неверное значение';
        this.isGetExecute = false;
        this.isSetExecute = false;
        this.isError = false;
        this.isRequired = false;
        this.condition = condition;
    }
    get descriptor() {
        return (target, propertyKey, descriptor) => {
            let oldSet = descriptor.set;
            let oldGet = descriptor.get;
            let filterThis = this;
            descriptor.set = function (value) {
                let object = this;
                object[ValidationModelSymbol] = object[ValidationModelSymbol] || {};
                let model = (object[ValidationModelSymbol][propertyKey] = ValidationInfoModel.createInstance(object[ValidationModelSymbol][propertyKey]));
                let filter = model.getOrAddFilter(filterThis);
                filter.isSetExecute = true;
                filter.isError = !filter.condition(value, this);
                model.isRequired = LINQ.fromArray(model.filters).any(m => m.isRequired);
                oldSet.call(this, value);
            };
            descriptor.get = function () {
                let value = oldGet.call(this);
                let object = this;
                object[ValidationModelSymbol] = object[ValidationModelSymbol] || {};
                let model = (object[ValidationModelSymbol][propertyKey] = ValidationInfoModel.createInstance(object[ValidationModelSymbol][propertyKey]));
                let filter = model.getOrAddFilter(filterThis);
                filter.isError = !filter.condition(value, this);
                filter.isGetExecute = true;
                model.isRequired = LINQ.fromArray(model.filters).any(m => m.isRequired);
                return value;
            };
            return descriptor;
        };
    }
}
export function Validate(m, propertyKey) {
    let object = m;
    object[ValidationModelSymbol] = object[ValidationModelSymbol] || {};
    if (propertyKey != null) {
        let model = (object[ValidationModelSymbol][propertyKey] = ValidationInfoModel.createInstance(object[ValidationModelSymbol][propertyKey]));
        return model;
    }
    else {
        let keys = Object.keys(object[ValidationModelSymbol]);
        return LINQ.fromArray(keys)
            .select(m => (object[ValidationModelSymbol][m] = ValidationInfoModel.createInstance(object[ValidationModelSymbol][m])))
            .selectMany(m => m.filters)
            .where(m => m.isError && m.isGetExecute);
    }
}
export function Property(...decorators) {
    return function (target, key) {
        let descriptor = Object.getOwnPropertyDescriptor(target, key) || {};
        delete target[key];
        descriptor.set = function (val) {
            this['_' + key] = val;
        };
        descriptor.get = function () {
            return this['_' + key];
        };
        for (let dec of decorators) {
            descriptor = dec(target, key, descriptor);
        }
        Object.defineProperty(target, key, descriptor);
    };
}
export function Custom(condition, error) {
    let filter = new FilterModel(condition);
    filter.errorText = error || filter.errorText;
    return filter.descriptor;
}
export function IsRequired(error) {
    let filter = new FilterModel(m => {
        if (typeof m == 'number')
            return m != null;
        return !!m;
    });
    filter.isRequired = true;
    filter.errorText = error || 'Поле обязательно для заполнения';
    return filter.descriptor;
}
export function IsRequiredWithCondition(cb, error) {
    let filter = new FilterModel((m, target) => {
        let res = cb(target, m);
        if (!res)
            return true;
        if (typeof m == 'number')
            return m != null;
        return !!m;
    });
    filter.isRequired = true;
    filter.errorText = error || 'Поле обязательно для заполнения';
    return filter.descriptor;
}
export function IsNotNull(error) {
    let filter = new FilterModel(m => {
        if (typeof m == 'number')
            return m != null;
        return !!m;
    });
    filter.errorText = error || 'Поле не должно быть пустым';
    return filter.descriptor;
}
export function All(error) {
    let filter = new FilterModel(m => {
        if (typeof m == 'number')
            return m != null;
        return !!m;
    });
    filter.errorText = error || 'Не все элементы удовлетворяют условию';
    return filter.descriptor;
}
export function IsPhone(error) {
    // let pattern = /^8\d{10}$/;
    let filter = new FilterModel(v => v
        ? `${v}`.startsWith('8') ||
            `${v}`.startsWith('7') ||
            `${v}`.startsWith('+7')
        : true);
    filter.errorText = error || 'Введите корректный номер телефона';
    return filter.descriptor;
}
export function Min(min, error) {
    let filter = new FilterModel((v) => (v ? v.length >= min : true));
    filter.errorText = error || `Минимальная длина ${min}`;
    return filter.descriptor;
}
export function Max(max, error) {
    let filter = new FilterModel((v) => (v ? v.length <= max : true));
    filter.errorText = error || `Максимальная длина ${max}`;
    return filter.descriptor;
}
export function Length(length, error) {
    let filter = new FilterModel((v) => (v ? v.length == length : true));
    filter.errorText = error || `Длина поля ${length}`;
    return filter.descriptor;
}
export function IsEmail(error) {
    let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Zа-яА-Я\-0-9]+\.)+[a-zA-Zа-яА-Я]{2,}))$/;
    let filter = new FilterModel(v => (v ? pattern.test(v) : true));
    filter.errorText = error || 'Введите корректный email-адрес';
    return filter.descriptor;
}
