import * as ringle from 'berish-ringle';
import guid from 'berish-guid';
import { promiseGet } from 'berish-promise-getter';
import { storageController, messagesController } from '.';
const DEFAULT_TIMEOUT = 150;
function setTimeoutPromise(timeout) {
    return new Promise(resolve => (timeout > 0 ? setTimeout(() => resolve(), timeout) : resolve()));
}
function executePromise(config, controller, funcHash) {
    return new Promise(async (resolve) => {
        let response = null;
        try {
            await setTimeoutPromise(config.timeout);
            let isExecute = controller.isExecute(funcHash);
            if (!isExecute)
                response = { status: 'stop', result: null };
            else {
                let result = await promiseGet(config.func);
                response = { status: 'done', result };
            }
        }
        catch (err) {
            response = { status: 'error', result: null, error: err };
        }
        delete controller.executeListeners[funcHash];
        resolve(response);
    });
}
function getConfig(config) {
    const DEBUG = config.debug;
    const defaultConfig = {
        func: DEBUG
            ? () => {
                console.log('it is test function executing');
                return null;
            }
            : () => {
                // EMPTY
                return null;
            },
        timeout: DEFAULT_TIMEOUT,
        setHash: DEBUG
            ? (funcHash, stopHash) => console.log(`funcHash ${funcHash}`, `stopHash ${stopHash}`)
            : () => {
                // EMPTY
            },
        tag: 'default'
    };
    return Object.assign({}, defaultConfig, config);
}
class ExecuteController {
    constructor() {
        this.executeListeners = {};
        this.tags = {};
    }
    executeStopRace(stopHash, funcHash) {
        return new Promise(resolve => {
            this.executeListeners[funcHash] = () => {
                delete this.executeListeners[funcHash];
                resolve({ status: 'stop', result: null });
            };
        });
    }
    async start(config) {
        config = getConfig(config);
        const stopHash = guid.guid();
        let funcHash = '';
        do
            funcHash = guid.guid();
        while (funcHash == stopHash);
        if (config.tag && config.stopByTag) {
            let tagArray = this.tags[config.tag] || [];
            let executeList = tagArray.map(m => this.isExecute(m));
            let isExecute = executeList.filter(m => !!m).length > 0;
            if (isExecute)
                this.stopAll(config.tag);
        }
        if (config.setHash)
            config.setHash(funcHash, stopHash);
        if (config.tag) {
            let tagArray = this.tags[config.tag] || [];
            tagArray.push(funcHash);
            this.tags[config.tag] = tagArray;
        }
        const res = await Promise.race([
            executePromise(config, this, funcHash),
            this.executeStopRace(stopHash, funcHash)
        ]);
        if (config.setHash)
            config.setHash(null, stopHash);
        if (config.tag) {
            let tagArray = this.tags[config.tag] || [];
            let index = tagArray.indexOf(funcHash);
            if (index != -1)
                tagArray.splice(index, 1);
            this.tags[config.tag] = tagArray;
        }
        if (res.status == 'stop')
            return null;
        if (res.status == 'done')
            return res.result;
        throw res.error;
    }
    stop(hash) {
        let listener = this.executeListeners[hash];
        if (listener) {
            listener();
            return hash;
        }
        return null;
    }
    stopAll(tag) {
        let hashes = Object.keys(this.executeListeners);
        if (tag) {
            let tagArray = this.tags[tag] || [];
            hashes = hashes.filter(m => tagArray.indexOf(m) != -1);
            this.tags[tag] = [];
        }
        return hashes.map(m => this.stop(m)).filter(m => m != null);
    }
    isExecute(funcHash) {
        const hashes = Array.isArray(funcHash) ? funcHash : [funcHash];
        const isExecute = hashes.map(m => !!this.executeListeners[m]).filter(m => m).length > 0;
        return isExecute;
    }
    async tryLoad(cb, config, ...args) {
        let result = null;
        config = config || {};
        config.disableLoading = config.disableLoading || false;
        config.disableModalError = config.disableModalError || false;
        config.allowException = config.allowException || false;
        config.executeTag = config.executeTag || guid.generateId();
        const systemStore = storageController.systemStore;
        let exception = null;
        try {
            if (!config.disableLoading) {
                if (config.changeLoading)
                    await config.changeLoading(true);
                else {
                    const action = systemStore.createMethod(m => {
                        if (config.title)
                            m.loadingTitle = config.title;
                        let loadingHash = m.loadingHash || [];
                        loadingHash.push(config.executeTag);
                        m.loadingHash = loadingHash;
                    });
                    systemStore.dispatch(action);
                }
            }
            result = await this.start({
                // func: () => (args && args.length > 0 ? cb(...args) : cb()),
                func: () => cb(...args),
                tag: config.executeTag,
                stopByTag: true
            });
        }
        catch (err) {
            if (process.env.NODE_ENV == 'development')
                console.error.call(this, err);
            if (!config.disableModalError) {
                messagesController.error(err);
            }
            if (config.allowException) {
                exception = err;
            }
        }
        finally {
            if (!config.disableLoading) {
                if (config.changeLoading)
                    await config.changeLoading(false);
                else {
                    const action = systemStore.createMethod(m => {
                        let loadingHash = m.loadingHash || [];
                        if (config.title == m.loadingTitle)
                            m.loadingTitle = null;
                        let index = loadingHash.indexOf(config.executeTag);
                        if (index != -1)
                            loadingHash.splice(index, 1);
                        m.loadingHash = loadingHash;
                    });
                    systemStore.dispatch(action);
                }
            }
            if (exception)
                throw exception;
            return result;
        }
    }
    async tryLoadNotification(cb, config, ...args) {
        let result = null;
        config = config || {};
        config.disableSuccessNotification =
            config.disableSuccessNotification || false;
        config.notification = config.notification || {
            message: config.title || 'Сохранение прошло успешно'
        };
        config.disableModalError =
            config.disableModalError == null ? true : config.disableModalError;
        config.allowException = true;
        try {
            result = await this.tryLoad(cb, config, ...args);
            if (!config.disableSuccessNotification)
                messagesController.notification(config.notification);
        }
        catch (err) {
            messagesController.error(err);
        }
        finally {
            return result;
        }
    }
}
export default ringle.getSingleton(ExecuteController);
