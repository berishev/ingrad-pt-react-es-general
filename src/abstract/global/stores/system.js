import { MethodStore } from '../redux-helper';
export class SystemStore extends MethodStore {
    get loadingHash() {
        return this.get('loadingHash') || [];
    }
    set loadingHash(value) {
        this.set('loadingHash', value);
    }
    get loadingTitle() {
        return this.get('loadingTitle') || '';
    }
    set loadingTitle(value) {
        this.set('loadingTitle', value);
    }
    get title() {
        return this.get('title') || '';
    }
    set title(value) {
        this.set('title', value);
    }
}
