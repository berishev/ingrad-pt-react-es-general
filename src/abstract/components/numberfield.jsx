import * as React from 'react';
import { Input, Icon } from '@material-ui/core';
import { AbstractComponent, Tooltip, FormItem } from './abstract';
class Textfield extends AbstractComponent {
    constructor(props) {
        super(props);
        this.clearValue = () => {
            this.onChange(null);
        };
        this.onChange = (value) => {
            if (!value && value != '0') {
                this.setValue(null);
            }
            else {
                let regExp = this.props.regExp;
                if (regExp) {
                    if (!regExp.test(`${value}`)) {
                        return;
                    }
                }
                let num = parseInt(value, 10);
                this.setValue(num);
            }
        };
        this.renderSuffix = (value) => {
            return value ? <Icon onClick={this.clearValue}>close-circle</Icon> : null;
        };
        this.state = {
            value: ''
        };
    }
    componentDidMount() {
        let value = this.getValue() || '';
        if (value != this.state.value)
            this.setState({ value });
    }
    componentWillReceiveProps(nextProps) {
        let value = this.getValue(null, nextProps) || '';
        if (value != this.state.value)
            this.setState({ value });
    }
    render() {
        const { value } = this.state;
        let placeholder = this.props.placeholder;
        let props = {
            placeholder,
            value,
            onChange: e => this.onChange(e.target.value),
            disabled: !!this.props.disabled,
            type: 'number',
            endAdornment: !!this.props.disabled ? undefined : this.renderSuffix(value)
        };
        let input = <Input {...props} fullWidth={true}/>;
        return Tooltip(input, {
            placeholder,
            trigger: 'focus'
        });
    }
}
export default FormItem(Textfield, { fiTitleType: 'InputLabel' });
