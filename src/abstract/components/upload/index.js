import UploadStandart from './standart';
import UploadButton from './uploadButton';
import UploadMethods from './uploadMethods';
export { UploadStandart, UploadButton, UploadMethods };
