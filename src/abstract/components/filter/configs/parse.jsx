import * as Parse from 'parse';
import { LINQ } from 'berish-linq';
import AbstractConfig from '../config';
export default class ParseAbstractConfig extends AbstractConfig {
    applyMethod() {
        let result = super.applyMethod();
        let queries = [];
        if (this.filters != null && this.filters.length > 0) {
            let filters = this.filters;
            queries = filters.map(filter => filter(this.clone(result)));
        }
        return queries.length <= 1 ? queries[0] : this.parseAnd(...queries);
    }
    parseOr(...data) {
        return ParseAbstractConfig.parseOr(...data);
    }
    parseAnd(...data) {
        return ParseAbstractConfig.parseAnd(...data);
    }
    parseGetIncludes(...data) {
        return ParseAbstractConfig.parseGetIncludes(...data);
    }
    static parseGetIncludes(...data) {
        return LINQ.fromArray(data || [])
            .selectMany(m => (m['_include'] || []))
            .distinct()
            .toArray();
    }
    static parseOr(...data) {
        let includes = this.parseGetIncludes(...data);
        let query = Parse.Query.or(...data);
        if (includes.length > 0)
            query = query.include(includes);
        return query;
    }
    static parseAnd(...data) {
        let includes = this.parseGetIncludes(...data);
        let query = Parse.Query['and'](...data);
        if (includes.length > 0)
            query = query.include(includes);
        return query;
    }
}
