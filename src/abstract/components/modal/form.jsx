import * as React from 'react';
import { Modal, withStyles } from '@material-ui/core';
import { storageController } from '../../global';
import styles from './styles';
import Spin from '../spin';
class ModalForm extends React.Component {
    constructor(props) {
        super(props);
        this.unlistener = storageController.systemStore.subscribe(m => {
            let loading = !!(m.loadingHash && m.loadingHash.length > 0);
            if (loading != this.state.loading)
                this.setState({ loading });
        });
        this.state = {
            loading: false
        };
    }
    render() {
        const { classes } = this.props;
        return (<Modal open={true} title={undefined} container={this.props.getContainer ? this.props.getContainer(null) : null} onClose={e => this.props.reject && this.props.reject()}>
        <div style={{
            maxWidth: '90vw',
            maxHeight: '80vh',
            overflowY: 'auto',
            width: this.props.width,
            top: `50%`,
            left: `50%`,
            transform: `translate(-50%, -50%)`
        }} className={classes.paper}>
          <Spin loading={this.state.loading}>{this.props.children}</Spin>
        </div>
      </Modal>);
    }
}
export default withStyles(styles)(ModalForm);
