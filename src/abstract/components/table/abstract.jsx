import * as React from 'react';
import { Checkbox } from '../';
import TableView from './view';
import { messagesController } from '../../global';
export const ON_PAGE_DEFAULT = 1000;
export default class AbstractTable extends React.Component {
    constructor(props) {
        super(props);
        this.load = null;
        // VIEW
        this.showAll = async () => {
            this.onChange({
                pageSize: this.state.showAll ? this.state.total : this.props.onPage || ON_PAGE_DEFAULT,
                total: this.state.total,
                current: this.state.showAll ? 1 : this.state.currentPage
            }, [], null);
        };
        this.onChange = async (pagination, filters, sorter) => {
            let currentPage = pagination.current;
            let pageSize = pagination.pageSize;
            let showData = await this.loadData(() => this.loadPage(currentPage, null, pageSize, this.state));
            this.setState({
                showData,
                currentPage
            });
        };
        this.rowSelect = (data) => {
            let onSelect = this.props.selection && this.props.selection.onSelect;
            if (onSelect)
                this.props.selection.on(data);
        };
        this.renderAll = (total, range) => {
            return (<div style={{ margin: '0 5px' }}>
        <Checkbox fiTitle="Показать все" placeholder="Показать все элементы" config={{
                object: this.state,
                path: 'showAll',
                afterSet: state => this.setState(state, this.showAll)
            }}/>
      </div>);
        };
        this.state = {
            loading: false,
            showData: [],
            total: 0,
            currentPage: 1,
            showAll: false,
            onPage: this.props.onPage || ON_PAGE_DEFAULT
        };
    }
    componentDidMount() {
        this.loadData(this.load);
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.data !== this.props.data) {
            this.loadData(() => this.load(nextProps));
        }
        // if (!_.isEqual(nextProps.data, this.props.data)) this.loadData(() => this.load(nextProps));
    }
    async loadData(promise) {
        const loading = (loading) => new Promise(resolve => this.setState({ loading }, resolve));
        try {
            await loading(true);
            let result = null;
            result = await promise();
            return result;
        }
        catch (err) {
            messagesController.error(err);
            return null;
        }
        finally {
            await loading(false);
        }
    }
    render() {
        const { showData, total, loading } = this.state;
        return (<TableView {...this.props} loading={loading} dataSource={showData || []} columns={this.props.columns || []} selection={{
            disabled: this.props.selection && this.props.selection.disabled,
            on: this.props.selection && this.props.selection.on,
            items: this.props.selection && this.props.selection.items
        }} containerMinusHeight={this.props.containerMinusHeight} onSort={this.sortPage}/>);
    }
}
