import guid from 'berish-guid';
import AbstractTable, { ON_PAGE_DEFAULT } from './abstract';
import parseQueryClone from '../../util/parseQueryClone';
import executeController from '../../global/executeController';
export default class QueryTable extends AbstractTable {
    constructor(props) {
        super(props);
        this.tableHash = guid.guid();
        this.load = async (nextProps) => {
            let { data } = nextProps || this.props;
            if (data == null)
                return;
            let { showData, total, currentPage } = this.state;
            currentPage = 1;
            const executionFunction = async () => {
                let total = await data.count();
                let showData = await this.loadPage(currentPage, nextProps);
                return { total, showData };
            };
            const result = await executeController.start({
                func: executionFunction,
                tag: this.tableHash,
                stopByTag: true
            });
            if (result) {
                total = result.total;
                showData = result.showData;
                this.setState({
                    showData,
                    total,
                    currentPage
                });
            }
        };
        this.loadPage = async (page, nextProps, pageSize) => {
            let { onPage, data, onChangePage } = nextProps || this.props;
            let { showData } = this.state;
            let limit = pageSize || onPage || ON_PAGE_DEFAULT;
            let query = parseQueryClone(data)
                .skip(limit * (page - 1))
                .limit(limit);
            if (onChangePage) {
                let changedQuery = await onChangePage(query);
                query = parseQueryClone(changedQuery);
            }
            showData = await query.find();
            return showData;
        };
        this.sortPage = (column, type) => {
            const _sortPage = async () => {
                let { data, ...props } = this.props;
                let { showData, currentPage } = this.state;
                let sorterKey = column.sorter();
                data = parseQueryClone(data);
                if (type == 'down')
                    data = data.ascending(sorterKey);
                else if (type == 'up')
                    data = data.descending(sorterKey);
                showData = await this.loadPage(currentPage, { ...props, data }, null);
                this.setState({ showData });
            };
            return this.loadData(_sortPage);
        };
    }
}
