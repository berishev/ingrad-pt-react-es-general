import * as React from 'react';
import * as Components from '../';
export default class TableModal extends React.Component {
    constructor(props) {
        super(props);
        this.resolve = () => {
            this.props.resolve(this.state.selected);
        };
        this.reject = () => {
            this.props.resolve();
        };
        this.renderFooterBar = () => {
            let left = [
                <Components.Button key="0" buttonTitle="Убрать элементы из выбранного" disabled={this.state.selected.length < 1} onClick={() => this.resolve()}/>
            ];
            return <Components.ButtonBar left={left}/>;
        };
        this.state = {
            selected: []
        };
    }
    render() {
        let TableClass = this.props.tableClass;
        return (<Components.Modal.Form resolve={this.resolve} reject={this.reject}>
        <Components.ContentForm title="Выбранные элементы" footerBar={this.renderFooterBar()}>
          <TableClass {...this.props.tableProps} selection={{
            selected: this.state.selected,
            onSelect: selected => this.setState({ selected }),
            disabledShowSelected: true
        }}/>
        </Components.ContentForm>
      </Components.Modal.Form>);
    }
}
