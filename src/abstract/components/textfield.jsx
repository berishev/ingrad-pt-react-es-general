import * as React from 'react';
import { Input } from '@material-ui/core';
import { Close } from '@material-ui/icons';
import { AbstractComponent, Tooltip, FormItem } from './abstract';
class Textfield extends AbstractComponent {
    constructor(props) {
        super(props);
        this.clearValue = () => {
            this.onChange('');
        };
        this.onChange = (value) => {
            let regExp = this.props.regExp;
            if (regExp) {
                if (!(regExp.test(value) || value === '')) {
                    return;
                }
            }
            this.setValue((value || '').replace(/^\s+/, ''));
        };
        this.renderSuffix = (value) => {
            return value ? <Close onClick={this.clearValue}/> : null;
        };
        this.state = {
            value: ''
        };
    }
    componentDidMount() {
        let value = this.getValue() || '';
        if (value != this.state.value)
            this.setState({ value });
    }
    componentWillReceiveProps(nextProps) {
        let value = this.getValue(null, nextProps) || '';
        if (value != this.state.value)
            this.setState({ value });
    }
    render() {
        const { value } = this.state;
        let placeholder = this.props.placeholder;
        let type = this.props.type || 'text';
        let props = {
            placeholder: this.props.usePasswordHint
                ? '********'
                : placeholder || null,
            value,
            onChange: e => this.onChange(e.target.value),
            onFocus: this.props.onFocus,
            onBlur: this.props.onBlur,
            onKeyPress: e => e.charCode == 13
                ? this.props.onPressEnter && this.props.onPressEnter()
                : true,
            disabled: !!this.props.disabled,
            type: type == 'password' ? 'password' : undefined,
            multiline: type == 'area' ? true : undefined,
            endAdornment: !!this.props.disabled ? undefined : this.renderSuffix(value)
        };
        let input = <Input {...props} fullWidth={true}/>;
        return Tooltip(input, {
            placeholder,
            trigger: 'hover'
        });
    }
}
export default FormItem(Textfield, { fiTitleType: 'InputLabel' });
