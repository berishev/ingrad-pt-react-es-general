import * as React from 'react';
import QueryLINQ from 'berish-parse-query-linq';
import { LINQ } from 'berish-linq/dist';
import * as Model from '../../model';
import { Selectfield } from '../../abstract/components';
import { AbstractComponent } from '../../abstract/components/abstract';
import executeController from '../../abstract/global/executeController';
export default class extends AbstractComponent {
    constructor(props) {
        super(props);
        this.onLoad = async (nextProps) => {
            let query = Model.SPOrgStructure.getQuery();
            let data = await QueryLINQ(query);
            this.setState({
                data
            });
        };
        this.renderData = () => {
            let { data } = this.state;
            return data.orderBy(m => m.spID).select(m => {
                return {
                    value: m,
                    view: `${m.spID} - ${m.LinkTitle}`
                };
            });
        };
        this.state = {
            data: LINQ.fromArray([])
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.onLoad);
    }
    render() {
        return (<Selectfield placeholder="Выберите SP-Оргструтуру" data={this.renderData()} {...this.props}/>);
    }
}
