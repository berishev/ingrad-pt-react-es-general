import * as React from 'react';
import { Selectfield } from '../../abstract/components';
import { AbstractComponent } from '../../abstract/components/abstract';
export default class TeamPositionType extends AbstractComponent {
    constructor(props) {
        super(props);
        this.renderData = () => {
            if (!this.props.team)
                return [];
            return this.props.team.positions.map(m => {
                return {
                    value: m,
                    view: `${this.props.team.name} - ${m}`
                };
            });
        };
    }
    render() {
        return (<Selectfield {...this.props} data={this.renderData()} placeholder="Выберите проектную роль"/>);
    }
}
