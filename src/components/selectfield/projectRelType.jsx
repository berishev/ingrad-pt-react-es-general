import * as React from 'react';
import * as Model from '../../model';
import { Selectfield } from '../../abstract/components';
import { AbstractComponent } from '../../abstract/components/abstract';
export default class ProjectRelType extends AbstractComponent {
    constructor(props) {
        super(props);
        this.renderData = () => {
            return Model.ProjectRelTypeLabels.toLinq().select(m => {
                return {
                    view: `${Model.ProjectRelTypeEnum[m.key]} - ${m.value}`,
                    value: m.key
                };
            });
        };
    }
    render() {
        return (<Selectfield {...this.props} data={this.renderData()} placeholder="Выберите город/область"/>);
    }
}
