import * as React from 'react';
import * as Parse from 'parse';
import QueryLINQ from 'berish-parse-query-linq';
import { LINQ } from 'berish-linq/dist';
import * as Model from '../../model';
import { Tagsfield } from '../../abstract/components';
import { AbstractComponent } from '../../abstract/components/abstract';
import executeController from '../../abstract/global/executeController';
export default class extends AbstractComponent {
    constructor(props) {
        super(props);
        this.onLoad = async (nextProps) => {
            let query = new Parse.Query(Model.Project);
            let data = await QueryLINQ(query);
            this.setState({
                data
            });
        };
        this.renderData = () => {
            let { data } = this.state;
            return data.select(m => {
                return {
                    value: m,
                    view: `${m.code} - ${m.name}`
                };
            });
        };
        this.state = {
            data: LINQ.fromArray([])
        };
    }
    componentDidMount() {
        executeController.tryLoad(this.onLoad);
    }
    render() {
        return (<Tagsfield placeholder="Выберите проекты" data={this.renderData()} {...this.props}/>);
    }
}
