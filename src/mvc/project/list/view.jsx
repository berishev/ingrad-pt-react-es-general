import * as React from 'react';
import * as Model from '../../../model';
import * as Components from '../../../abstract/components';
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.renderColumns = () => {
            let columns = [
                {
                    title: 'Код',
                    render: item => item.code
                },
                {
                    title: 'Наименование',
                    render: item => item.name
                },
                {
                    title: 'Коммерческое наименование',
                    render: item => item.commercialName
                },
                {
                    title: 'Иконка',
                    render: item => (item.iconUrl && (<Components.Image style={{ width: 30, height: 30 }} placeholder={item.name || item.code} value={item.iconUrl}/>)) ||
                        item.code
                },
                {
                    title: 'Город',
                    render: item => item.relType != null
                        ? Model.ProjectRelTypeLabels.get(item.relType)
                        : null
                }
            ];
            return columns;
        };
    }
    render() {
        let { state, onSelect } = this.props.controller;
        let { query, selected } = state;
        return (<Components.ContentForm>
        <Components.Table.QueryTable data={query} columns={this.renderColumns()} selection={{ items: selected, on: onSelect, disabled: true }} columnStyle={{ fontSize: 19 }} rowStyle={{ fontSize: 17 }}/>
      </Components.ContentForm>);
    }
}
